package com.room.rest.api.stations.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.room.rest.api.rooms.domain.Room;
import com.room.rest.api.stations.service.StationsService;


@Controller
@RequestMapping(value = "/api/stations")
public class StationsController {
	
	@Autowired
	private StationsService stationsService;
	
	private static final Logger logger = LoggerFactory.getLogger(StationsController.class);
	
	
	//지하철역으로 방을 검색
	@ResponseBody
	@RequestMapping(value = "/{stationId}/room", method = RequestMethod.GET)
	public ResponseEntity<List<Room>> modifyRoom(@PathVariable(value="stationId") String stationId, @RequestParam(value="distance") String distance) {
		logger.info("Welcome home! The client locale is {}.");
		//지하철역ID와 거리를 LESS로 해서 매핑테이블에서 모든 매물 조회
		List<Room> roomList = null;
		stationsService.inquiryStation(stationId, distance);
		
		return new ResponseEntity<List<Room>>(roomList, HttpStatus.OK);
	}
	
}
