package com.room.rest.api.buildings.domain;

import java.util.Date;

public class Building {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column building.id
     *
     * @mbggenerated Sat Feb 07 21:47:48 KST 2015
     */
    private Integer id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column building.name
     *
     * @mbggenerated Sat Feb 07 21:47:48 KST 2015
     */
    private String name;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column building.address
     *
     * @mbggenerated Sat Feb 07 21:47:48 KST 2015
     */
    private String address;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column building.floors
     *
     * @mbggenerated Sat Feb 07 21:47:48 KST 2015
     */
    private String floors;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column building.number_of_households
     *
     * @mbggenerated Sat Feb 07 21:47:48 KST 2015
     */
    private Integer numberOfHouseholds;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column building.completion_date
     *
     * @mbggenerated Sat Feb 07 21:47:48 KST 2015
     */
    private Date completionDate;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column building.id
     *
     * @return the value of building.id
     *
     * @mbggenerated Sat Feb 07 21:47:48 KST 2015
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column building.id
     *
     * @param id the value for building.id
     *
     * @mbggenerated Sat Feb 07 21:47:48 KST 2015
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column building.name
     *
     * @return the value of building.name
     *
     * @mbggenerated Sat Feb 07 21:47:48 KST 2015
     */
    public String getName() {
        return name;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column building.name
     *
     * @param name the value for building.name
     *
     * @mbggenerated Sat Feb 07 21:47:48 KST 2015
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column building.address
     *
     * @return the value of building.address
     *
     * @mbggenerated Sat Feb 07 21:47:48 KST 2015
     */
    public String getAddress() {
        return address;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column building.address
     *
     * @param address the value for building.address
     *
     * @mbggenerated Sat Feb 07 21:47:48 KST 2015
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column building.floors
     *
     * @return the value of building.floors
     *
     * @mbggenerated Sat Feb 07 21:47:48 KST 2015
     */
    public String getFloors() {
        return floors;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column building.floors
     *
     * @param floors the value for building.floors
     *
     * @mbggenerated Sat Feb 07 21:47:48 KST 2015
     */
    public void setFloors(String floors) {
        this.floors = floors;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column building.number_of_households
     *
     * @return the value of building.number_of_households
     *
     * @mbggenerated Sat Feb 07 21:47:48 KST 2015
     */
    public Integer getNumberOfHouseholds() {
        return numberOfHouseholds;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column building.number_of_households
     *
     * @param numberOfHouseholds the value for building.number_of_households
     *
     * @mbggenerated Sat Feb 07 21:47:48 KST 2015
     */
    public void setNumberOfHouseholds(Integer numberOfHouseholds) {
        this.numberOfHouseholds = numberOfHouseholds;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column building.completion_date
     *
     * @return the value of building.completion_date
     *
     * @mbggenerated Sat Feb 07 21:47:48 KST 2015
     */
    public Date getCompletionDate() {
        return completionDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column building.completion_date
     *
     * @param completionDate the value for building.completion_date
     *
     * @mbggenerated Sat Feb 07 21:47:48 KST 2015
     */
    public void setCompletionDate(Date completionDate) {
        this.completionDate = completionDate;
    }
}