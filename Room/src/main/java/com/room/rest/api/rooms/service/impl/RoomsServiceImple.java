package com.room.rest.api.rooms.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.room.rest.api.rooms.dao.RoomMapper;
import com.room.rest.api.rooms.domain.Room;
import com.room.rest.api.rooms.domain.RoomExample;
import com.room.rest.api.rooms.service.RoomsService;

@Service
public class RoomsServiceImple implements RoomsService {

	@Autowired
	private RoomMapper roomMapper;

	public RoomsServiceImple() {

	}

	@Override
	public List<Room> inquiryRooms() {
		RoomExample r = new RoomExample();
		r.createCriteria().andDepositGreaterThan(3000);
		r.createCriteria().andTitleLike("착하신");
		List<Room> list = roomMapper.selectByExample(r);
		return list;
	}
	
	@Override
	public int createRoom(Room room) {
		int result = roomMapper.insert(room);
		return result;
	}

	@Override
	public Room inquiryRoom(int roomId) {
		Room room = roomMapper.selectByPrimaryKey(roomId);
		return room;
	}


	@Override
	public int deleteRoom(int roomId) {
		int result = roomMapper.deleteByPrimaryKey(roomId);
		return result;
	}

	@Override
	public int modifyRoom(Room room) {
		int result = roomMapper.updateByPrimaryKey(room);
		return result;
	}
	
	public void inquiryRoomsByCondition(Room room){
		RoomExample r = new RoomExample();
		 List<Room> asdf =  roomMapper.selectByExample(new RoomExample());
	}

}
