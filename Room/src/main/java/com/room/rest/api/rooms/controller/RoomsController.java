package com.room.rest.api.rooms.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.room.rest.api.rooms.domain.Room;
import com.room.rest.api.rooms.service.RoomsService;

@Controller
@RequestMapping(value = "/api/rooms")
public class RoomsController {
	
	@Autowired
	private RoomsService roomService;
	
	private static final Logger logger = LoggerFactory.getLogger(RoomsController.class);
	
	@ResponseBody
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ResponseEntity<List<Room>> inquiryRooms() {
		List<Room> list = roomService.inquiryRooms();
		logger.info("inquiryRooms");
		
		return new ResponseEntity<List<Room>>(list, HttpStatus.OK);
	}
	
	@ResponseBody
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public String createRoom(@RequestBody Room room) {
		roomService.createRoom(room);
		logger.info("createRoom");
		
		
		return "room is created.";
	}
	@ResponseBody
	@RequestMapping(value = "/{roomId}", method = RequestMethod.GET)
	public ResponseEntity<Room> inquiryRoom(@PathVariable(value="roomId") int roomId) {
		Room room = roomService.inquiryRoom(roomId);
		logger.info("inquiryRoom");
		
		return new ResponseEntity<Room>(room, HttpStatus.OK);
	}
	@ResponseBody
	@RequestMapping(value = "/", method = RequestMethod.PUT)
	public String modifyRoom(@RequestBody Room room) {
		logger.info("modifyRoom");
		int result = roomService.modifyRoom(room);
		
		return result + "room is modified.";
	}
	@ResponseBody
	@RequestMapping(value = "/{roomId}", method = RequestMethod.DELETE)
	public String deleteRoom(@PathVariable(value="roomId") int roomId) {
		logger.info("deleteRoom");
		int result = roomService.deleteRoom(roomId);
		
		return result + "room is deleted.";
	}
	
}
