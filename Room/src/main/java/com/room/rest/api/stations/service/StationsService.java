package com.room.rest.api.stations.service;

import com.room.rest.api.stations.domain.Station;

public interface StationsService {
	Station inquiryStation(String stationId, String distance);

}
