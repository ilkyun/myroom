package com.room.rest.api.rooms.service;

import java.util.List;

import com.room.rest.api.rooms.domain.Room;


public interface RoomsService {

	public List<Room> inquiryRooms();

	public int createRoom(Room room);

	public Room inquiryRoom(int roomId);

	public int modifyRoom(Room room);

	public int deleteRoom(int roomId);

}
