CREATE TABLE `rooms`.`agent` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '중개소ID',
  `number` VARCHAR(30) CHARACTER SET 'utf8' NULL COMMENT '중개사번호',
  `address` VARCHAR(300) NOT NULL COMMENT '주소',
  `name` VARCHAR(100) NOT NULL COMMENT '상호명',
  `owner_name` VARCHAR(45) NOT NULL COMMENT '대표명',
  `phone_number` INT(11) NOT NULL COMMENT '대표번호',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = '중개소';

CREATE TABLE `rooms`.`user` (
  `id` INT NOT NULL COMMENT '고객ID',
  `name` VARCHAR(45) NULL COMMENT '이름',
  `cellphone_number` INT(11) NULL COMMENT '전화번호',
  `email` VARCHAR(45) NULL COMMENT '이메일',
  `password` VARCHAR(100) NOT NULL COMMENT '비밀번호',
  `kakaotalk_id` VARCHAR(45) NULL COMMENT '카카오톡ID',
  `agent_id` INT NULL COMMENT '중개소ID',
  PRIMARY KEY (`id`),
  FOREIGN KEY (agent_id) references agent(id)
  on update cascade)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = '사용자';

CREATE TABLE `rooms`.`room` (
  `id` INT NOT NULL COMMENT '매물ID',
  `title` VARCHAR(300) NULL COMMENT '제목',
  `images` VARCHAR(150) NULL COMMENT '사진',
  `deposit` INT NULL COMMENT '보증금',
  `monthly_rent` INT NULL COMMENT '월세',
  `type` VARCHAR(45) NULL COMMENT '방종류(오피스텔, 아파트)',
  `address` VARCHAR(450) NULL COMMENT '주소',
  `size` INT NULL COMMENT '크기',
  `floors` INT NULL COMMENT '층수',
  `structure` VARCHAR(45) NULL COMMENT '구조',
  `options` VARCHAR(90) NULL COMMENT '옵션',
  `management_expenses` VARCHAR(45) NULL COMMENT '관리비',
  `management_included_item` VARCHAR(150) NULL COMMENT '관리포함항목',
  `park` INT NULL COMMENT '주차',
  `elevator` VARCHAR(45) NULL COMMENT '엘레베이터',
  `possible_movein_date` DATETIME NULL COMMENT '입주가능일',
  `pet` INT NULL COMMENT '애완동물',
  `heating` VARCHAR(45) NULL COMMENT '난방종류',
  `description` VARCHAR(3000) NULL COMMENT '상세설명',
  `location` VARCHAR(450) NULL COMMENT '위치(지도)',
  `user_id` INT NULL COMMENT '중개사ID',
  `building_id` INT NULL COMMENT '건물ID',
  `state` VARCHAR(45) NULL COMMENT '상태',
  PRIMARY KEY (`id`),
  FOREIGN KEY (user_id) references rooms.userroom(id),
  FOREIGN KEY (building_id) references building(id)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = '매물';

CREATE TABLE `rooms`.`room_station_mapping` (
  `id` INT NOT NULL COMMENT '매물지하철역ID',
  `room_id` INT NULL COMMENT '매물ID',
  `station_id` INT NULL COMMENT '지하철역ID',
  `distance` INT NULL COMMENT '거리(5분, 10분, 15분)',
  PRIMARY KEY (`id`),
  FOREIGN KEY (room_id) references room(id),
  FOREIGN KEY (station_id) references station(id))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = '매물지하철역 ';

CREATE TABLE `rooms`.`building` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '건물ID',
  `name` VARCHAR(45) NULL COMMENT '건물이름',
  `address` VARCHAR(300) NULL COMMENT '주소',
  `floors` VARCHAR(45) NULL COMMENT '층수',
  `number_of_households` INT NULL COMMENT '세대수',
  `completion_date` DATETIME NULL COMMENT '건출년월',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = '건물';

CREATE TABLE `rooms`.`station` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '역ID',
  `name` VARCHAR(45) NULL COMMENT '역이름',
  `lines` VARCHAR(45) NULL COMMENT '호선(1:N)',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = '지하철역';



